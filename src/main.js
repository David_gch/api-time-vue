import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import VueAxios from 'vue-axios';
import axios from 'axios';
import {store} from './store/store';
import { i18n } from './traduccion/i18n';

Vue.use(BootstrapVue, VueAxios, axios)

new Vue({
  el: '#app',
  store,
  i18n,
  render: h => h(App)
})

