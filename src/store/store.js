import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        provinciaActual: 0,
        municipios: null,
        tiempos: null,
    }
  });
  